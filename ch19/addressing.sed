# Apply command on line 2.
2s/fox/cat/

# Apply command on line 2 to line 4.
2,4s/dog/gorilla/

# Apply command on line 3 to last line.
2,$s/brown/blue/

# Apply command on a line with victorhuberta.
/victorhuberta/s/Victor Huberta/The Devil of Hell's Kitchen/

# Apply multiple commands on a line with victorhuberta.
/victorhuberta/{
    s/victorhuberta/daredevil/
    s/bash/csh/
}

# Apply multiple commands on line 2.
2{
    s/bash/falseeeeeeeeeee/
    s/bin/binnnnnnnnnnnnnn/
}
