# Append a line after the first line.
1a\This is an appended line #1.

# Insert a line before the fourth line and the last line.
4,$i\This is an inserted line #1.

# Append multiple lines after the second line.
2a\
This is an appended line #2.\
This is an appended line #3.
