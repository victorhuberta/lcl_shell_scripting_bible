# MAWK: Show users' shells.

BEGIN {
    print "This is the latest users and shells listing."
    print " UserID \t  Shell"
    print "-------- \t --------"
    FS=":"
}

{
    print $1 "         \t " $7
}

END {
    print "This is the end of the listing."
}
