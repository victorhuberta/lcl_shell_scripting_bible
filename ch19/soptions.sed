# All occurrences of victorhuberta
s/victorhuberta/censored/g

# Print the line to STDOUT
s/root/toor/p

# Only the second occurrence of censored.
s/censored/test/2

# Print the line to a file.
s/censored/doraemon/w test1.txt

# Change all /bin/bash to /bin/csh.
s!/bin/bash!/bin/csh!g
